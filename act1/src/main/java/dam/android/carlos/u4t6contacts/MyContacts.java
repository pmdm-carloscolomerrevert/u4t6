package dam.android.carlos.u4t6contacts;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class MyContacts {
    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context){
        this.context=context;
        this.myDataSet=getContacts();
    }

    //Get contacts list from ContactsProvider
    private ArrayList<ContactItem>getContacts(){
        ArrayList<ContactItem> contactsList = new ArrayList<>();

        //accesss to ContentProviders
        ContentResolver contentResolver = context.getContentResolver();

        //TODO 1: Modificamos la consulta para que nos muestre los datos que queremos
        //aux variables
        String[] projection = new String[]{ ContactsContract.Data._ID, ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY, ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.Data.DATA2, ContactsContract.Data.PHOTO_THUMBNAIL_URI};


        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";


        //query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection, selectionFilter, null, ContactsContract.Data.DISPLAY_NAME + " ASC");

        if(contactsCursor != null){
            //get the column indexes for desired Name and Number columns
            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int rawContactId = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
       //     int phoneType = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.)
            int photoThumbnailUri = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);

            //read data and add to ArrayList
            while(contactsCursor.moveToNext()){
                String id = contactsCursor.getString(idIndex);
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String rawContact = contactsCursor.getString(rawContactId);
                String photoUri = contactsCursor.getString(photoThumbnailUri);

                ContactItem ci = new ContactItem(id, name, number, photoUri);
                Log.i("log", "getContacts " + ci.getNombre());
                contactsList.add(ci);

            }
            contactsCursor.close();
        }


        return contactsList;
    }

    public ContactItem getContactData(int position){
        return myDataSet.get(position);
    }
    public int getCount(){
        return myDataSet.size();
    }

}
