package dam.android.carlos.u4t6contacts;
//TODO 1: Creamos la clase contactItem para almecenar los datos de cada contacto en una lista
public class ContactItem {

    private String id, nombre, numero, imagen;

    public ContactItem(String id, String nombre, String numero, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

}
