package dam.android.carlos.u4t6contacts;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private MyContacts myContacts;

    //Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public MyViewHolder(TextView textView){
            super(textView);
            this.textView=textView;
        }

        //sets viewHolder views with data
        public void bind(String contactData){
            this.textView.setText(contactData);
        }
    }

    //consturctor: myContacts contains Contacts data
    MyAdapter(MyContacts myContacts){
        this.myContacts=myContacts;
    }

    //Creates new view item: Layout Manager calls hia metthod
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        //Create item View:
        // use a simple TextView predefined layout (sdk/platforms/android-xx/data/res/layout) thay contains only TextView
        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new MyViewHolder(tv);
    }

    //replaces the data context of a viewholder (recycles ald viewholder): Layout Manager calls this method

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       //bind viewHolder with data at: position
        holder.bind(myContacts.getContactData(position));
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }
}
