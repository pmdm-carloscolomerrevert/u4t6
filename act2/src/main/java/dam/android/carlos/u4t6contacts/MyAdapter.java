package dam.android.carlos.u4t6contacts;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface OnItemClickListener{
        void onItemClick(String activityName);
    }

    private ArrayList<ContactItem> myContacts;
    private OnItemClickListener listener;


    //Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView id, nombre, numero;
        ImageView imagen;

        public MyViewHolder(View itemView){
            super(itemView);
            this.id=itemView.findViewById(R.id._ID);
            this.nombre = itemView.findViewById(R.id.nombreContacto);
            this.numero = itemView.findViewById(R.id.numTelefono);
            this.imagen = itemView.findViewById(R.id.imageView);

        }

        //sets viewHolder views with data
        public void bind(ArrayList<ContactItem> contactData, int position, OnItemClickListener listener){
            this.id.setText(contactData.get(position).getId());
            this.nombre.setText(contactData.get(position).getNombre());
            this.numero.setText(contactData.get(position).getNumero());
            String uri = contactData.get(position).getImagen();
            if (uri != null){
                Uri photoUri = Uri.parse(uri);
                this.imagen.setImageURI(photoUri);
            }

            //TODO 2: añadimos el listener del nombre del contacto
            this.nombre.setOnClickListener(v -> listener.onItemClick(id.getText().toString()));
        }
    }

    //consturctor: myContacts contains Contacts data
    MyAdapter(ArrayList<ContactItem> myContacts, OnItemClickListener listener){
        this.myContacts=myContacts;
        this.listener=listener;


    }

    //Creates new view item: Layout Manager calls hia metthod
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        //Create item View:
        // use a simple TextView predefined layout (sdk/platforms/android-xx/data/res/layout) thay contains only TextView
      //  TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items, parent, false);
        return new MyViewHolder(view);
    }

    //replaces the data context of a viewholder (recycles ald viewholder): Layout Manager calls this method

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       //bind viewHolder with data at: position
        holder.bind(myContacts, position, listener);
    }

    @Override
    public int getItemCount() {
        return myContacts.size();
    }
}
