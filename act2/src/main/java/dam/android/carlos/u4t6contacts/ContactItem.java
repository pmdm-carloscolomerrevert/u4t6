package dam.android.carlos.u4t6contacts;
//TODO 1: Creamos la clase contactItem para almecenar los datos de cada contacto en una lista
public class ContactItem {

    private String id, nombre, numero, contactId, lookupKey, rawConctactId, typePhone, imagen;

    public ContactItem(String id, String nombre, String numero, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
    }

    public ContactItem(String id, String nombre, String numero, String contactId, String lookupKey, String rawConctactId, String typePhone, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.contactId = contactId;
        this.lookupKey = lookupKey;
        this.rawConctactId = rawConctactId;
        this.typePhone = typePhone;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getRawConctactId() {
        return rawConctactId;
    }

    public void setRawConctactId(String rawConctactId) {
        this.rawConctactId = rawConctactId;
    }

    public String getTypePhone() {
        return typePhone;
    }

    public void setTypePhone(String typePhone) {
        this.typePhone = typePhone;
    }

    @Override
    public String toString() {
        return  nombre + " " + numero + " ("+ typePhone + ")\n"+
                " _ID: " + id + " CONTACT_ID: " + contactId +
                " RAW_CONTACT_ID: " + rawConctactId +
                " LOOKUP_KEY: " + lookupKey;
    }
}
