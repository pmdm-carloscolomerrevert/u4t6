package dam.android.carlos.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.MotionEventCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {
    ArrayList<ContactItem> myContacts;
    MyContacts contactos;
    RecyclerView recyclerView;
    ConstraintLayout contacto;
    TextView tvInfoContacto;
    ConstraintLayout clListaContactos, clContacto;


    //Permissions required to contacts provider,, only needed to READ
    private static String[] PERMISSIONS_CONTACTS = {
            Manifest.permission.READ_CONTACTS
    };

    // Id to identify a contacts permission requiest
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
        if (checkPermissions()) {
            setListAdapter();
        }

    }


    private void setUI() {
        contacto = findViewById(R.id.clContacto);
        tvInfoContacto = findViewById(R.id.tvInfoContacto);
        clContacto = findViewById(R.id.clContacto);
        clListaContactos = findViewById(R.id.clListaContactos);
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        //set recyclerView with a linear layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        //TODO 1: separador entre filas
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        //TODO 2: ocultar textView cuando se haga movimiento sobre la lista
        recyclerView.addOnScrollListener(new CustomScrollListener());


    }

//TODO 2: al pulsar sobre el nombre del contacto, nos muestra un textView con la información del contacto
    @Override
    public void onItemClick(String activityName) {

        String contact = "";
        for (ContactItem contactos : myContacts) {
            contact = contactos.getId();
            if (contact.equals(activityName)) {
                tvInfoContacto.setVisibility(View.VISIBLE);
                tvInfoContacto.setText(contactos.toString());
            }
        }
    }

    public class CustomScrollListener extends RecyclerView.OnScrollListener {
        public CustomScrollListener() {
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            switch (newState) {

                case RecyclerView.SCROLL_STATE_SETTLING:
                    tvInfoContacto.setVisibility(View.INVISIBLE);
                    break;

            }
        }
    }


    private void setListAdapter() {
        //MyContacts class gets data from ContactsProvider
        myContacts = new ArrayList<>();
        contactos = new MyContacts(this);

        for (int i = 0; i < contactos.getCount(); i++) {

            myContacts.add(contactos.getContactData(i));
        }


        //set adapter to recyclerview
        recyclerView.setAdapter(new MyAdapter(myContacts, this));

        //Hide empty list TextView
        if (myContacts.size() > 0) {
            findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
        }

    }


    private boolean checkPermissions() {
        //Check permissions granted before setting listView Adapter data
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            //opens dialog: request user to grant permission
            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CONTACTS) {
            // We have requested one READ permission for contacts, so only need[0] to be checked
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setListAdapter();
            } else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
